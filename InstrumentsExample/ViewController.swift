//
//  ViewController.swift
//  InstrumentsExample
//
//  Created by Alok Deepti on 20/07/19.
//  Copyright © 2019 Alok. All rights reserved.
// SvitlanaTest

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func mainclick(_ sender: Any) {
        
        for i in 0...10000000{
            print("value is \(i)")
        }
        
        let urlStr = "https://photojournal.jpl.nasa.gov/images/ppj_hp_pluto.jpg"
        let url = URL(string: urlStr)
        
        if url != nil {
            
            let data = try? Data(contentsOf: url!)
            
            if data != nil{
                let image = UIImage(data: data!)
                imgView.image = image
            }
            
        }
        
    }
    
    @IBAction func bgClicked(_ sender: Any) {
        
        DispatchQueue.global().async {
            
            for i in 0...1000000{
                
                DispatchQueue.main.async {
                    print("value is \(i)")
                }
            }
            
            let urlStr = "https://photojournal.jpl.nasa.gov/images/ppj_hp_pluto.jpg"
            let url = URL(string: urlStr)
            
            if url != nil {
                
                let data = try? Data(contentsOf: url!)
                
                if data != nil{
                    let image = UIImage(data: data!)
                    DispatchQueue.main.async {
                    self.imgView.image = image
                    }
                }
                
            }
        }
    }
    
    
    @IBAction func openNewScreen(_ sender: Any) {
        
        let detailVC = DetailViewController()
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
    


}

