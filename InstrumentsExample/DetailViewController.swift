//
//  DetailViewController.swift
//  InstrumentsExample
//
//  Created by Alok Deepti on 20/07/19.
//  Copyright © 2019 Alok. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var person : Person?
    var address : Address?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blue
        
        person = Person()
        address = Address()
        
        person?.address = address
        address?.person = person

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
